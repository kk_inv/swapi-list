FROM node:7.10.0

ENV NPM_CONFIG_LOGLEVEL warn

COPY . .

RUN npm install

RUN npm run build --production

RUN npm install -g serve

CMD serve -s build --port 8890
EXPOSE 8890


