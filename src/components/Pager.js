import React, { Component } from 'react'
import { Link } from 'react-router-dom'

class Pager extends Component {
  render () {
    var next = this.props.curpage * 1 + 1
    var previous = this.props.curpage * 1 - 1 || 1
    return (
      <div className='Pager'>
        <Link className='pagerbutton btn btn-success' to={`/lista/${previous}`}>Previous</Link>
      #{this.props.curpage}
        <Link className='pagerbutton btn btn-success' to={`/lista/${next}`}>Next</Link>
      </div>
    )
  }
}

export default Pager
