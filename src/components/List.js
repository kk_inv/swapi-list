import React from 'react'

import Actors from './Actors'
import Pager from './Pager'
import Top from './Top'

export default class List extends React.Component {
  render () {
    return (
      <div>
        <Top curpage={this.props.match.params.listpage} />
        <Actors curpage={this.props.match.params.listpage} />
        <Pager curpage={this.props.match.params.listpage} />
      </div>
    )
  };
}
