import React from 'react'
import { Switch, Route, Redirect } from 'react-router-dom'

import List from './List'

const Main = () => (
  <main>
    <Switch>
      <Redirect from='/' exact to='/lista/1' />
      <Redirect from='/lista' exact to='/lista/1' />
      <Route path='/lista/:listpage' component={List} />

    </Switch>
  </main>
)

export default Main
