import React, { Component } from 'react'
import fetch from 'isomorphic-fetch'
import promise from 'es6-promise'
import { peopleStore } from '../stores/PeopleStore'
import { observer } from 'mobx-react';

promise.polyfill()

const apiUrl = 'https://swapi.co/api/'

@observer
export default class Actors extends Component {

  constructor (props) {
    super(props)
    this.getActors()
  }

  componentWillReceiveProps (nextProps) {

	this.getActors()
  }

  getActors () {
	  var listpage = this.props.curpage || 1
	    peopleStore.loadPeople(listpage)
  }


  render () {
    if (peopleStore.isLoading) {
      return <div className='loadingspin'>Loading ...</div>
    }
    return (
      <table className='table table-bordered table-striped TableFilms'>
        <thead>
          <tr>
            <th>Full Name</th>
            <th>Films</th>
          </tr>
        </thead>
        <tbody>
          { peopleStore.people
               .map(person => <tr key={person.name}><td>{person.name}</td>
                 <td>
                 {person.films.map(film => <p key={film}>{film}</p>)}
                 </td></tr>
                )}
        </tbody></table>
    )

  }
}
