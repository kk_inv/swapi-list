import React, { Component } from 'react'



class Top extends Component {
  render () {
    return (
      <div className='Films'>
        <div className='Films-header'>

          <img className='LogoImg' src={require('../../public/star_wars_logo.png')} />
          <h2>Welcome to Star Wars People list Page #{this.props.curpage}, - 1.0.2</h2>

        </div>
      </div>
    )
  }
}

export default Top
