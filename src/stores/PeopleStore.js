import { observable, computed, action } from "mobx";
import { observer } from "mobx-react";

const api_url = 'https://swapi.co/api/';
const check1 = 1;

class PeopleStore {
	@observable people = [];
	@observable isLoading = 1;


	@action
	loadPeople(listpage) {
		this.people = []
		this.isLoading = 1
		fetch(api_url + 'people/?format=json&page=' + listpage)
			.then(response => response.json())
			.then(data => {
				this.addPeople(data.results);
				data.results.forEach((person, i) => {
					person.films.forEach((film, it) => {
						fetch(film)
							.then(d => d.json())
							.then(d => {
								this.people[i].films[it] = d.title
								console.log(this.people)
								this.isLoading = 0
							}).catch(error => console.error(error))
					})
				})
			}).catch(error => console.error(error))
	}


	getPeople() {
		return this.people
	}

	addPeople(list) {
		this.people = list;
	}

}


export const peopleStore = new PeopleStore();