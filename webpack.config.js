const webpack = require('webpack')
const path = require('path')

const HOST = process.env.HOST || 'localhost'
const PORT = process.env.PORT || '8080'
const ExtractTextPlugin = require('extract-text-webpack-plugin')

const HtmlWebpackPlugin = require('html-webpack-plugin')
const HtmlWebpackPluginConfig = new HtmlWebpackPlugin({
  template: __dirname + '/public/index.html',
  filename: 'index.html',
  inject: 'body'
})

module.exports = {
  entry: ['babel-polyfill', './src/index.js'],
  output: {
    path: __dirname + '/build',
    filename: 'bundle.js',
    publicPath: "/"
  },
  devServer: {
    contentBase: __dirname + '/public'

  },

  devtool: 'source-map',
  module: {

    loaders: [
    {
      test: /\.js$/,
      exclude: /(node_modules|bower_components)/,
      use: {
        loader: 'babel-loader',
        options: {
          plugins: ['transform-runtime', 'transform-decorators-legacy'],
          presets: ['es2015', 'stage-0', 'react']
        }
      }
    },
    { test: /\.(png|jpg|gif)$/, loader: "file-loader?name=img/img-[hash:6].[ext]" },
    {
      test: /\.css$/,
      use: [ 'style-loader', 'css-loader' ]
    }
  ],
},
  plugins: [HtmlWebpackPluginConfig]
}
